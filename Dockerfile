FROM gitlab-registry.cern.ch/hep-benchmarks/hep-workloads/hep-workloads-base:slc6

COPY conda.repo /etc/yum/repos.d/conda.repo

RUN rpm --import https://repo.anaconda.com/pkgs/misc/gpgkeys/anaconda.asc

RUN yum install -y conda && yum clean all 

COPY conda-spec-file.txt /etc/conda/conda-spec-file.txt
COPY TD.npz /bmk/data/TD.npz
COPY data_generation.pickle /bmk/data/data_generation.pickle
COPY config_complete.ini /bmk/data/config_complete.ini
COPY run_igwn_bmk.sh /bmk/bin/run_igwn_bmk.sh
COPY create_summary.py /bmk/bin/create_summary.py

ARG CONDAPATH=/opt/conda

RUN ${CONDAPATH}/bin/conda create -y --copy -n igwn-hepscore-bilby-bmk --file /etc/conda/conda-spec-file.txt && \
    ${CONDAPATH}/bin/conda clean --all --force-pkgs-dirs --yes && \
    mkdir -p ${CONDAPATH}/pkgs/ && \
    touch ${CONDAPATH}/pkgs/urls.txt

# Try to silence messages from PhenomXPHM about falling back from MSA
ENV LAL_DEBUG_LEVEL 0
