#!/usr/bin/env bash

source /opt/conda/etc/profile.d/conda.sh
conda activate igwn-hepscore-bilby-bmk

bilby_label="igwn_hepscore_bilby_bmk"

cd /tmp
bilby_pipe_analysis "/bmk/data/config_complete.ini" --outdir "$PWD" --detectors H1 --detectors L1 --label "${bilby_label}" --data-dump-file "/bmk/data/data_generation.pickle" --sampler dynesty
status=${?}
if [ "x$status" != "x0" ]; then
    exit $status
else
    /bmk/bin/create_summary.py -p "result/${bilby_label}_dynesty.pickle" -r "result/${bilby_label}_result.json" -o "bmk.json"
    status=${?}
fi

exit $status
