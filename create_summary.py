#!/usr/bin/env python3

import dill
import json
import argparse

parser = argparse.ArgumentParser(description="Calculate figure-of-merit"
                                 " from a bilby HEP-Score benchmark job")
parser.add_argument("-p", "--pickle-file", type=str, required=True,
                    help="Path to dynesty pickle file from"
                    " bilby_pipe_analysis")
parser.add_argument("-r", "--result-file", type=str, required=True,
                    help="Path to bilby_pipe_analysis result JSON file")
parser.add_argument("-o", "--output-file", type=str, required=True,
                    help="Path to which to write JSON output")

args = parser.parse_args()

with open(args.pickle_file, "rb") as ff:
    data = dill.load(ff)
    nlikelihood = sum(data['ncall'])

with open(args.result_file, "r") as ff:
    json_obj = json.load(ff)
    sampling_time = json_obj['sampling_time']

evals_per_sec = nlikelihood/sampling_time
outdict = {'evals_per_sec': evals_per_sec}

with open(args.output_file, "w") as ff:
    json.dump(outdict, ff)

